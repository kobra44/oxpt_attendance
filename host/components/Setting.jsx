import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

import IconButton from '@material-ui/core/IconButton'
import SettingsIcon from '@material-ui/icons/Settings'
import InputAdornment from '@material-ui/core/InputAdornment'
import Switch from '@material-ui/core/Switch'
import Grid from '@material-ui/core/Grid'

import i18nInstance from '../i18n'

export default () => {
  const { locales, page, pushState } = useStore()

  if (!(locales))
    return <></>

  const [t, i18n] = useTranslation('translations', { i18nInstance })

  const languageVariablesObject = (key) => {
    return (typeof locales[key].translations.variables) === 'object' ? locales[key].translations.variables : {}
  }

  const langKeys = Object.keys(locales)
  const initUnits = langKeys.reduce((units, key) => ({
    ...units,
    [key]: languageVariablesObject(key)
  }), {})
  const [localesTemp] = useState(initUnits)
  const [open, setOpen] = useState(page === 'instruction' || page === 'waiting')

  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const handleOnCancel = () => {
    setOpen(false)
  }

  const handleOnSend = () => {
    pushState({
      event: 'setting',
      payload: {
        locales_temp: localesTemp
      },
      callback: () => {}
    })
    setOpen(false)
  }

  return (
    <>
      <IconButton aria-label={t('host.setting.title_01')} value="setting" disabled={page !== 'instruction' && page !== 'waiting'} onClick={setOpen.bind(null, true)}>
        <SettingsIcon />
      </IconButton>
      <Dialog
        onClose={setOpen.bind(null, false)}
        open={open}
        fullWidth={true}
        maxWidth="xl">
        <DialogTitle>{t('host.setting.title_01')}</DialogTitle>
        <DialogContent>
          <TableContainer component={Paper}>
            <Table size="small">
              <TableHead>
                <TableRow>
                  <TableCell align="center">項目</TableCell>
                  <TableCell align="center">情報</TableCell>
                  <TableCell align="center">編集</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell align="left">
                    QRコード入力
                  </TableCell>
                  <TableCell align="center">
                    必要
                  </TableCell>
                  <TableCell align="center">
                    <Grid component="label" container alignItems="center">
                      <Grid item>不要</Grid>
                      <Grid item>
                        <Switch checked={false} name="checked" />
                      </Grid>
                      <Grid item>必要</Grid>
                    </Grid>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell align="left">
                    QRコード更新間隔
                  </TableCell>
                  <TableCell align="center">
                    3秒
                  </TableCell>
                  <TableCell align="center">
                    <TextField
                      value="3"
                      InputProps={{
                        endAdornment: <InputAdornment position="end">秒</InputAdornment>
                      }}/>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell align="left">
                    QRコード有効期限
                  </TableCell>
                  <TableCell align="center">
                    3分
                  </TableCell>
                  <TableCell align="center">
                    <TextField value="3"
                      InputProps={{
                        endAdornment: <InputAdornment position="end">分</InputAdornment>
                      }}/>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell align="left">
                    イメージ画像
                  </TableCell>
                  <TableCell align="center">
                    ファイル名
                  </TableCell>
                  <TableCell align="center">
                    <Button variant="outlined">画像ファイル選択</Button>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell align="left">
                    入力情報
                  </TableCell>
                  <TableCell align="center">
                    学生証番号
                  </TableCell>
                  <TableCell align="center">
                    <TextField
                      value="学生証番号"
                    />
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleOnCancel}>
            {t('host.setting.cancel_01')}
          </Button>
          <Button onClick={handleOnSend} color="primary" disabled={page !== 'instruction' && page !== 'waiting'}>
            {t('host.setting.send_01')}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}
