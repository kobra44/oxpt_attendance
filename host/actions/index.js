import { createAction, handleActions } from 'redux-actions'
import { fromSnakeToCamel } from '../util'
export const defaultState = {
}

export const PUSH_STATE = 'PUSH_STATE'
export const UPDATE_STATE = 'UPDATE_STATE'
export const pushState = createAction(PUSH_STATE)
export const updateState = createAction(UPDATE_STATE)

export const reducer = handleActions(
  {
    [PUSH_STATE]: (state, _action) => state,
    [UPDATE_STATE]: (state, action) => {
      return { ...state, ...fromSnakeToCamel(action.payload) }
    }
  },
  defaultState
)
