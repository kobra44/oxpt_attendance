import 'babel-polyfill'
import { takeEvery, put, call } from 'redux-saga/effects'
import { eventChannel } from 'redux-saga'
import channel from 'oxpt'
import * as Actions from '../actions'
import { fromCamelToSnake } from '../util'

function subscribe() {
  return eventChannel(emit => {
    channel.on('update_state', ({ state }) => {
      emit(Actions.updateState(state))
    })
    return () => {}
  })
}

function * receiveSocket(action) {
  yield put(action)
}

function * sendData(action) {
  const { event, payload, callback } = action.payload
  setTimeout(() => {
    if (payload === undefined || payload === null) {
      channel.push('input', {
        event: event,
        payload: null
      })
    } else {
      channel.push('input', {
        event: event,
        payload: fromCamelToSnake(payload)
      })
    }
    if (typeof callback === 'function') callback('success')
  }, 1000)
}

export default function * root() {
  const chan = yield call(subscribe)
  yield takeEvery(chan, receiveSocket)
  yield takeEvery(Actions.PUSH_STATE, sendData)
}
