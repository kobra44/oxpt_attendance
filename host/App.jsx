import React, { useReducer, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useStore } from './actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles, useTheme } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Slider from '@material-ui/core/Slider'

import i18nInstance from './i18n'
import Setting from './components/Setting'
import GuestTable from './components/GuestTable'

import { QRCode } from 'react-qrcode-logo'

const useStyles = makeStyles(theme => ({
  title: {
    padding: 0,
    paddingRight: theme.spacing(1),
    paddingLeft: theme.spacing(1),
  },
  items: {
    paddingTop: theme.spacing(1),
    paddingRight: 0,
    paddingLeft: 0
  },
  slider: {
    padding: theme.spacing(1)
  }
}))

const QRCodes = ({ gameId, expirationTime, imageSize }) => {
  let value = gameId + ':' + expirationTime
  console.log(value)
  return (
    <QRCode
      value={value}
      size={imageSize}
      ecLevel="H"
      qrStyle="dots"
    />
  )
}

const App = () => {
  const classes = useStyles()
  const theme = useTheme()
  const { locales, hostGameId } = useStore()
  const [ expirationTime, setExpirationTime ] = useState(Date.now())
  const [ imageSize, setImageSize ] = useState(document.body.clientWidth / 2)
  let interval = 3 // seconds
  let sliderWidth = document.body.clientWidth - theme.spacing(2)

  useEffect(() => {
    setTimeout(() => {
      setExpirationTime(expirationTime + interval * 1000)
    }, interval * 1000)
  })

  const [, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const handleChangeSize = (event, newValue) => {
    setImageSize(newValue)
  }

  if (!(locales))
    return <></>

  return (
    <>
      <Grid item xs={12} className={classes.title}>
        <Typography variant="h5">
          Attendance
        </Typography>
      </Grid>
      <Grid item xs={12} className={classes.slider}>
        画像サイズ
        <Slider value={imageSize} onChange={handleChangeSize} max={sliderWidth} mix={0}/>
      </Grid>
      <Grid item xs={12} className={classes.items}>
        <QRCodes expirationTime={expirationTime} gameId={hostGameId} imageSize={imageSize} />
      </Grid>
      <Grid item xs={12} className={classes.items}>
        <GuestTable />
      </Grid>
      <Grid item xs={12} className={classes.items}>
        <Setting />
      </Grid>
    </>
  )
}

QRCodes.propTypes = {
  gameId: PropTypes.string,
  pass: PropTypes.string,
  time: PropTypes.string,
  imageSize: PropTypes.number,
  expirationTime: PropTypes.number
}

export default App
