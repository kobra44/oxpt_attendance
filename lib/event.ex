defmodule Oxpt.Attendance.Events do
  @moduledoc """
  Documentation for Oxpt.Attendance.Events

  gameで使うイベントを定義する
  """

  defmodule UpdateStateAll do
    @moduledoc """
    すべてのplayer_socketがsubscribeしておくべきイベント。
    全員に対しあるstateを送りたいときに使う

    """
    defstruct [:game_id, :state]
  end

  defmodule UpdateStateSome do
    @moduledoc """
    guest_idsで指定されたゲストのplayer_socketがsubscribeしておくべきイベント。
    そのゲストに対しあるstateを送りたいときに使う

    """
    defstruct [:guest_ids, :game_id, :state]
  end

  defmodule FetchState do
    @moduledoc """
    stateを管理しているオートマトンがsubscribeしておくべきイベント。
    player_socketがspawnした時にDispatchし、クライアントにstateを送る

    """
    defstruct [:game_id, :guest_id]
  end

  defmodule Read do
    defstruct  [:game_id, :guest_id]
  end

  defmodule ChangePage do
    defstruct [:game_id, :page]
  end

  defmodule ChangeSetting do
    defstruct [:game_id, :payload]
  end

  defmodule ChangeShared do
    @moduledoc """
    例1: すべてのguestが共有するstateを変更
    現在のページ、ゲームの設定などはこのパターン

    """
    defstruct [:game_id, :value]
  end

  defmodule ChangeOwn do
    @moduledoc """
    例2: あるguestのstateを変更
    大体はこのパターン

    """
    defstruct [:game_id, :guest_id, :value]
  end
end
