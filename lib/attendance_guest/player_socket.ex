defmodule Oxpt.Attendance.Guest.PlayerSocket do
  use Cizen.Automaton
  defstruct [:game_id, :guest_id]

  use Cizen.Effects
  alias Cizen.{Filter, Event}
  alias Oxpt.Player.{Input, Output}

  alias Oxpt.Attendance.Events.{
    UpdateStateSome,
    UpdateStateAll,
    FetchState,
    Read,
    ChangeShared,
    ChangeOwn
  }

  @impl true
  def spawn(id, %__MODULE__{} = socket) do
    perform(id, %Subscribe{
      event_filter:
        Filter.new(fn
          %Event{
            body: %UpdateStateAll{game_id: ^socket.game_id}
          } ->
            true

          %Event{
            body: %UpdateStateSome{game_id: ^socket.game_id, guest_ids: ids}
          } ->
            Enum.member?(ids, socket.guest_id)

          %Event{
            body: %Input{event: "read", guest_id: ^socket.guest_id}
          } ->
            true

          %Event{
            body: %Input{event: "change shared", guest_id: ^socket.guest_id}
          } ->
            true

          %Event{
            body: %Input{event: "change own", guest_id: ^socket.guest_id}
          } ->
            true
        end)
    })

    perform(id, %Dispatch{
      body: %FetchState{
        game_id: socket.game_id,
        guest_id: socket.guest_id
      }
    })

    {:loop, socket}
  end

  @impl true
  def yield(id, {:loop, socket}) do
    event = perform(id, %Receive{})

    case event.body do
      %Input{} = input ->
        handle_input(id, input, socket)

      %UpdateStateAll{state: state} ->
        dispatch_state(id, state, socket)

      %UpdateStateSome{state: state} ->
        dispatch_state(id, state, socket)
    end

    {:loop, socket}
  end

  def handle_input(id, %Input{event: "read" }, socket) do
    perform(id, %Dispatch{
      body: %Read{
        game_id: socket.game_id,
        guest_id: socket.guest_id
      }
    })
  end

  def handle_input(id, %Input{event: "change shared", payload: value}, socket) do
    perform(id, %Dispatch{
      body: %ChangeShared{
        game_id: socket.game_id,
        value: value
      }
    })
  end

  def handle_input(id, %Input{event: "change own", payload: value}, socket) do
    perform(id, %Dispatch{
      body: %ChangeOwn{
        game_id: socket.game_id,
        guest_id: socket.guest_id,
        value: value
      }
    })
  end


  defp dispatch_state(id, state, socket) do
    perform(id, %Dispatch{
      body: %Output{
        game_id: socket.game_id,
        guest_id: socket.guest_id,
        event: "update_state",
        payload: %{state: get_merge_data(state, socket.guest_id)}
      }
    })
  end

  defp get_merge_data(state, guest_id) do
    player = get_in(state, [:players, guest_id]) || %{}
    #group = get_in(state, [:groups, get_in(player, [:group_id])])

    # new_state = if state.page != "result" do
    #   state
    #   |> Map.drop([:players])
    #   #|> Map.drop([:players, :groups])
    #   #|> Map.merge(if group, do: %{group: group}, else: %{})
    # else
    #   state
    # end

    state |> Map.merge(player)
  end

end
