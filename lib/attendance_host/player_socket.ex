defmodule Oxpt.Attendance.Host.PlayerSocket do
  use Cizen.Automaton
  defstruct [:game_id, :guest_game_id, :guest_id]

  use Cizen.Effects
  alias Cizen.{Filter, Event}
  alias Oxpt.Player.{Input, Output}

  alias Oxpt.Attendance.Events.{
    UpdateStateSome,
    UpdateStateAll,
    ChangePage,
    FetchState,
    ChangeSetting
  }

  @impl true
  def spawn(id, %__MODULE__{} = socket) do
    perform(id, %Subscribe{
      event_filter:
        Filter.new(fn
          %Event{
            body: %UpdateStateAll{game_id: ^socket.guest_game_id}
          } ->
            true

          %Event{
            body: %UpdateStateSome{game_id: ^socket.guest_game_id}
          } ->
            true

          %Event{
            body: %Input{event: "change page", guest_id: ^socket.guest_id}
          } ->
            true

          %Event{
            body: %Input{event: "setting", guest_id: ^socket.guest_id}
          } ->
            true
        end)
    })

    perform(id, %Dispatch{
      body: %FetchState{
        game_id: socket.guest_game_id,
        guest_id: socket.guest_id
      }
    })

    {:loop, socket}
  end

  @impl true
  def yield(id, {:loop, socket}) do
    event = perform(id, %Receive{})

    case event.body do
      %Input{} = input ->
        handle_input(id, input, socket)

      %UpdateStateAll{state: state} ->
        dispatch_state(id, state, socket)

      %UpdateStateSome{state: state} ->
        dispatch_state(id, state, socket)
    end

    {:loop, socket}
  end

  defp dispatch_state(id, state, socket) do
    perform(id, %Dispatch{
      body: %Output{
        game_id: socket.game_id,
        guest_id: socket.guest_id,
        event: "update_state",
        payload: %{state: state}
      }
    })
  end

  defp handle_input(id, %Input{event: "change page", payload: page}, socket) do
    perform(id, %Dispatch{
      body: %ChangePage{
        game_id: socket.guest_game_id,
        page: page
      }
    })
  end

  defp handle_input(id, %Input{event: "setting", payload: payload}, socket) do
    perform(id, %Dispatch{
      body: %ChangeSetting{
        game_id: socket.guest_game_id,
        payload: payload
      }
    })
  end
end
