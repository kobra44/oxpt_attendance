import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from './actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Hidden from '@material-ui/core/Hidden'
import Typography from '@material-ui/core/Typography'

import Instruction from './pages/Instruction'

import i18nInstance from './i18n'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(1)
  },
  outer: {
    margin: 0,
    padding: 0
  }
}))

const CantJoinWaiting = ({ classes, t }) => (
  <Grid
    container
    direction="column"
    justify="flex-start"
    alignItems="stretch"
    className={classes.outer}
  >
    <Grid item sx={12} sm={10}>
      <Typography variant="body1">{t('guest.experiment.error.cant_join_01')}</Typography>
    </Grid>
  </Grid>
)

function renderPage(page) {
  switch (page) {
    case 'instruction':
      return <Instruction />
  }
}

const App = () => {
  const classes = useStyles()
  const { page, locales } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(page && locales))
    return <></>

  return (
    <Grid
      container
      direction="row"
      justify="space-between"
      alignItems="flex-start"
    >
      <Hidden xsDown><Grid item sm={1}></Grid></Hidden>
      <Grid item xs={12} sm={10}>
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="stretch"
        >
          <Grid item className={classes.items}>
            <Typography variant='h5'>
              {t('host.title_01')}
            </Typography>
          </Grid>
          <Grid item className={classes.items}>
            {renderPage(page)}
          </Grid>
        </Grid>
      </Grid>
      <Hidden xsDown><Grid item sm={1}></Grid></Hidden>
    </Grid>
  )
}

CantJoinWaiting.propTypes = {
  classes: PropTypes.object,
  t: PropTypes.object
}

renderPage.propTypes = {
  classes: PropTypes.object,
  t: PropTypes.object
}

export default App
